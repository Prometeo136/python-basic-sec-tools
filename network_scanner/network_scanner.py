import scapy.all as scapy

# def scan(ip):
#     arp_request = scapy.ARP(pdst=ip)
#     print(arp_request.summary())
    # scapy.ls(scapy.ARP()) --> is of all the fields we can set

def scan(ip):
    arp_request = scapy.ARP(pdst=ip)
    # arp_request.show()
    broadcast = scapy.Ether(dst='ff:ff:ff:ff:ff:ff')
    # broadcast.show()
    arp_request_broadcast = broadcast/arp_request
    # print(arp_request_broadcast.summary())
    # arp_request_broadcast.show()
    # scapy.ls(scapy.Ether())
    answered_list = scapy.srp(arp_request_broadcast, timeout=1)[0]

    clients_list = []
    for i in answered_list:
        client_dict = {"ip": i[1].psrc, "mac": i[1].hwsrc}
        clients_list.append(client_dict)
    return clients_list
    # print(answered_list.summary())
    # print(unanswered.summary())

def print_result(results_list):
    print("IP\t\t\tMAC Address\n---------------------------------------------------")
    for client in results_list:
        print(client["ip"] + "\t\t" + client["mac"])


if __name__ == '__main__':
    clients_list = scan("192.168.1.8/24")
    print_result(clients_list)
